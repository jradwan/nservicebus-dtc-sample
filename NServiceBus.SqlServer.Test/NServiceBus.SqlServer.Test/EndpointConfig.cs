
namespace NServiceBus.SqlServer.Test
{
    using NServiceBus;

	/*
		This class configures this endpoint as a Server. More information about how to configure the NServiceBus host
		can be found here: http://particular.net/articles/the-nservicebus-host
	*/
    public class EndpointConfig : IConfigureThisEndpoint, AsA_Server, UsingTransport<SqlServer>, IWantCustomInitialization
	{
        public void Init()
        {
            Configure.With().DefaultBuilder()
                .UseInMemoryTimeoutPersister()
                .InMemoryFaultManagement()
                .InMemorySubscriptionStorage();

            Configure.Transactions.Advanced(settings => settings.DisableDistributedTransactions());
        }
	}
}
