﻿using System.Configuration;
using System.Data.SqlClient;
using Messages;

namespace NServiceBus.Test.Sender
{
    class Program
    {
        static void Main(string[] args)
        {
            var bus = Configure.With()
                .DefaultBuilder()
               .DefineEndpointName("NServiceBus.SqlServer.Test.Sender")
               .UseTransport<SqlServer>()
               .UnicastBus()
               .InMemoryFaultManagement()
               .InMemorySubscriptionStorage()
               .UseInMemoryTimeoutPersister()
               .SendOnly();

            var unitofWork = Configure.Instance.Builder.Build<Transports.SQLServer.UnitOfWork>();

            //First scope of work
            using (var connection = new SqlConnection(ConfigurationManager.ConnectionStrings["NServiceBus/Transport"].ConnectionString))
            {
                connection.Open();
                var transaction = connection.BeginTransaction();
                unitofWork.SetTransaction(transaction);
                //Do DB work
                bus.Send("NServiceBus.SqlServer.Test", new TestMessage());
                transaction.Commit();
                unitofWork.ClearTransaction();
            }

            //Second scope of work
            //This simulates using the same thread to send a message across differnet scope of work
            //I expected the transport to create a new connection to send this message
            bus.Send("NServiceBus.SqlServer.Test", new TestMessage());
        }
    }
}
