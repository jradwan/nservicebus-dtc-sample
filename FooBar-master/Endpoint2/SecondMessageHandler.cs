﻿using System;
using MyMessages;
using NServiceBus;

namespace Endpoint2
{
    public class SecondMessageHandler: IHandleMessages<SecondMessage>
    {
        public void Handle(SecondMessage message)
        {
            throw new Exception("Transaction rollback failed.... We should never get this message.");
        }
    }
}
