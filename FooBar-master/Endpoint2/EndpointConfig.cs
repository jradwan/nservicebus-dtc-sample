
using NServiceBus.Features;

namespace Endpoint2
{
    using NServiceBus;


    [EndpointName("Endpoint2")]
    public class EndpointConfig : IConfigureThisEndpoint, IWantCustomInitialization, AsA_Server,
                                  UsingTransport<SqlServer>
    {
        public void Init()
        {
            Configure.Transactions.Advanced(settings => settings.DisableDistributedTransactions());

            Configure.With()
                     .DefaultBuilder()
                     .InMemorySubscriptionStorage()
                     .UseInMemoryTimeoutPersister();

            Configure.Features.Disable<SecondLevelRetries>();
            Configure.Features.Disable<AutoSubscribe>();
        }
    }
}
