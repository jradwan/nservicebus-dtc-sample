using System;
using System.Configuration;
using System.Data.SqlClient;
using System.Transactions;
using MyMessages;
using NServiceBus;
using NServiceBus.Saga;
using NServiceBus.Transports.SQLServer;
using IsolationLevel = System.Data.IsolationLevel;

namespace MyServer
{
    public class MySaga : Saga<MySaga.MySagaData>, IAmStartedByMessages<FirstMessage>, IHandleTimeouts<TimeMessage>
    {
        public UnitOfWork UnitOfWork { get; set; }
        public class MySagaData : ContainSagaData
        {
            public virtual string Name { get; set; }
        }

        public void Handle(FirstMessage message)
        {
            Console.Out.WriteLine("Saga started");

            using (var sqlConnection =
            new SqlConnection(ConfigurationManager.ConnectionStrings["NServiceBus/Transport"].ConnectionString))
            {
                sqlConnection.Open();
                var trans = sqlConnection.BeginTransaction(IsolationLevel.ReadCommitted);
                var mycommand2 = new SqlCommand("INSERT INTO MyTable VALUES(@Id,@Name)", sqlConnection, trans);

                mycommand2.Parameters.Add(new SqlParameter("Id", Guid.NewGuid()));
                mycommand2.Parameters.Add(new SqlParameter("Name", "Bob test 2 " + Data.Id));
                mycommand2.ExecuteNonQuery();
                trans.Commit();
            }
            Data.Name = message.Name;

            Bus.Send("Endpoint2", new SecondMessage { Name = "Second Message from: " + Data.Id });

            RequestTimeout<TimeMessage>(TimeSpan.FromSeconds(2));

            throw new Exception();


        }

        public void Timeout(TimeMessage state)
        {
            MarkAsComplete();
        }
    }
}